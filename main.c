#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>

int main(void) {
  const int tcp_sock = socket(AF_INET, SOCK_STREAM, 0);

  if (tcp_sock == -1) {
    perror("socket(...) error:");
    return EXIT_FAILURE;
  }


  struct sockaddr_in serv_addr = { 0 };

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(3000);
  serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);


  if (bind(tcp_sock, (struct sockaddr*) &serv_addr,
        sizeof(serv_addr)) == -1)
  {
    perror("bind(...) error:");
    return EXIT_FAILURE;
  }


  const int MAX_CONN = 50;

  if (listen(tcp_sock, MAX_CONN) == -1) {
    perror("listen(...) error:");
    return EXIT_FAILURE;
  }


  struct sockaddr_in addr;
  socklen_t addrlen = sizeof(addr);
  
  if (getsockname(tcp_sock, (struct sockaddr*) &addr, &addrlen) == -1) {
    perror("getsockname(...) error:");
    return EXIT_FAILURE;
  }

  printf("Listening on port %d...\n", ntohs(addr.sin_port));


  int req_sock = accept(tcp_sock, NULL, NULL);

  if (req_sock == -1) {
    perror("accept(...) error:");
    return EXIT_FAILURE;
  }


  char BUF[8] = { 0 };
  ssize_t bytes = 0;

  do {
    bytes = read(req_sock, BUF, sizeof(BUF) - 1);

    if (bytes < 0) {
      perror("read(...) error:");
      return EXIT_FAILURE;
    }


    for (ssize_t i = 0; i < bytes; i++) {
      BUF[i] = toupper(BUF[i]);
    }

    BUF[bytes] = 0;


    dprintf(req_sock, "%s", BUF);
  } while (bytes > 0);


  close(tcp_sock);
  close(req_sock);

  
  return EXIT_SUCCESS;
}
